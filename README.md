# helloasso-export

Simple HTTP API to export HelloAsso survey responses

## Setup

You first need to create the configuration file:

```bash
cp config.example.php config.php
```

Then edit `config.php` to add your credentials and a secret token that will be required to call this API.

You also need to install dependencies with [Composer](https://getcomposer.org/):

```bash
composer install
```

## Usage

Call `http://example.com/helloasso-export/csv/[id_evt]?token=[token]`.

(You can get `id_evt` in the URL of the CSV file when you download it manually from HelloAsso.)

This will make the script connect to HelloAsso, get the data of the specified event and return it as a CSV file compatible with Google Sheet.

## Grunt tasks

[Grunt](https://gruntjs.com/) can be used to run some automated tasks defined in `Gruntfile.js`.

Your first need to install JavaScript dependencies with [Yarn](https://yarnpkg.com/):

```bash
yarn install
```

### Lint

You can check that the JavaScript, JSON and PHP files are formatted correctly:

```bash
grunt lint
```

### Tests

You can run [PHPUnit](https://phpunit.de/) tests:

```bash
grunt test
```

### Documentation

[phpDocumentor](https://phpdoc.org/) can be used to generate the code documentation:

```bash
grunt doc
```

## CI

[Gitlab CI](https://docs.gitlab.com/ee/ci/) is used to run the tests automatically after each commit.
