<?php
/**
 * CsvParser class
 */
namespace HelloassoExport;

use League\Csv\Reader;
use League\Csv\Writer;

/**
 * Manipulate CSV files imported from HelloAsso
 */
class CsvParser
{

    /**
     * Dummy function that always returns true
     * @return true
     * @see https://csv.thephpleague.com/8.0/reading/#readereach
     */
    public static function true()
    {
        return true;
    }

    /**
     * Get a CSV export from HelloAsso
     * @param string $id Form ID
     * @return string CSV
     */
    public function getCsv($id)
    {
        $guzzle = new \GuzzleHttp\Client(['cookies' => true]);

        $guzzle->request('POST', 'https://www.helloasso.com/utilisateur/authentificate', [
            'form_params' => [
                'email' => HELLOASSO_USER,
                'password' => HELLOASSO_PASS,
                'showSp' => true
            ]
        ]);

        $date = new \DateTime();

        $csv = $guzzle->request(
            'GET',
            'https://www.helloasso.com/admin/handler/reports.ashx?'.http_build_query(
                [
                    'type'=>'Details',
                    'id_evt'=>$id,
                    'from'=>'1/1/1970',
                    'to'=>$date->format('j/n/Y'),
                    'includeSubpages'=>true,
                    'period'=>'MONTH',
                    'domain'=>'HelloAsso',
                    'trans'=>'Billets',
                    'format'=>'Csv'
                ]
            ),
            ['headers' => ['Accept-Encoding' => 'gzip']]
        )->getBody()->getContents();

        $reader = Reader::createFromString($csv);
        $reader->setDelimiter(';');

        $writer = Writer::createFromFileObject(new \SplTempFileObject());
        $writer->setDelimiter(',');
        $writer->insertOne($reader->fetchOne(0));
        for ($i = ($reader->each([$this, 'true']) - 1); $i > 0; $i--) {
            $writer->insertOne($reader->fetchOne($i));
        }

        return (string) $writer;
    }
}
