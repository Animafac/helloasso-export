<?php
/**
 * ExportController class
 */
namespace HelloassoExport\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use GuzzleHttp\Exception\ServerException;
use HelloassoExport\CsvParser;

/**
 * Controller used for CSV exports
 */
class ExportController
{

    /**
     * Output survey responses into a CSV file
     * @param  Request  $request  HTTP request
     * @param  Response $response HTTP response
     * @param  array    $args     Additional arguments specified in URL
     * @return Response
     */
    public function exportCsv(Request $request, Response $response, array $args)
    {

        $params = $request->getParams();
        if (!isset($params['token']) || $params['token'] != TOKEN) {
            $response->getBody()->write("Invalid token");

            return $response->withStatus(403)->withHeader('Content-type', 'text/plain');
        }

        $parser = new CsvParser();

        try {
            $response->getBody()->write($parser->getCsv($args['id']));

            return $response->withHeader('Content-Type', 'text/csv');
        } catch (ServerException $e) {
            $response->getBody()->write("Can't find this form");

            return $response->withStatus(404)->withHeader('Content-Type', 'text/plain');
        }
    }
}
