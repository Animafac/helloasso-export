<?php
/**
 * CsvParserTest class
 */
namespace HelloassoExport\Test;

use HelloassoExport\CsvParser;

/**
 * Test the CsvParser class
 */
class CsvParserTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Test the getCsv() function with a wrong login
     * @return void
     * @expectedException GuzzleHttp\Exception\ClientException
     */
    public function testGetCsvWithWrongLogin()
    {
        /**
         * HelloAsso username
         */
        define('HELLOASSO_USER', 'foo');
        /**
         * HelloAsso password
         */
        define('HELLOASSO_PASS', 'bar');
        $parser = new CsvParser($csv);
        $parser->getCsv(1);
    }

    /**
     * Test the getCsv() function
     * @return void
     */
    public function testGetCsv()
    {
        $this->markTestIncomplete('We need a way to test with a correct login.');
    }

    /**
     * Test the true() function
     * @return void
     */
    public function testTrue()
    {
        $this->assertTrue(CsvParser::true());
    }
}
