<?php
/**
 * ExportControllerTest class
 */
namespace HelloassoExport\Test;

use HelloassoExport\Controller\ExportController;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Test the ExportController class
 */
class ExportControllerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Initialize variables used in tests
     */
    protected function setUp()
    {
        $this->controller = new ExportController();
        $this->request = Request::createFromEnvironment(Environment::mock());
        $this->response = new Response();
    }

    /**
     * Test the exportCsv() function without a valid token
     * @return void
     */
    public function testExportCsvWithoutToken()
    {
        $result = $this->controller->exportCsv($this->request, $this->response, []);
        $this->assertTrue($result->isForbidden());
    }

    /**
     * Test the exportCsv() function without a valid login
     * @return void
     * @expectedException GuzzleHttp\Exception\ClientException
     */
    public function testExportCsvWithoutLogin()
    {
        define('TOKEN', 'foo');
        $result = $this->controller->exportCsv(
            $this->request->withQueryParams(['token'=>'foo']),
            $this->response,
            ['id'=>1]
        );
    }

    /**
     * Test the exportCsv() function
     * @return void
     */
    public function testExportCsv()
    {
        $this->markTestIncomplete('We need a way to test with a correct login.');
    }
}
